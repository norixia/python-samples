# Task: Create a game of Rock-Paper-Scissors
# src: https://www.youtube.com/watch?v=tMM93Fi_BAs
""" Rules:
     - Scissors beats Paper
     - Rock beats Scissors
     - Paper beats Rock
     - both players choose the same -> draw

    Two players:
     - Player A (player) must enter an input
     - Player B (pc) chooses randomly
"""
import random as r

rps = ["Rock", "Paper", "Scissors"]

# PC choice
pc_choice = r.choice(rps)
# print(pc_choice)

# Player choice
player_choice = input("Rock, Paper, Scissors? \n")
user_input = False

def Choice(player_choice):
     """Rock"""
     if player_choice == "Rock" and pc_choice == "Rock":
          print("It's a draw!")
     elif player_choice == "Rock" and pc_choice == "Scissors":
          print("You win!", player_choice, "smashes", pc_choice)
     elif player_choice == "Rock" and pc_choice == "Paper":
          print("You lose!", pc_choice, "covers", player_choice)

     """Paper"""
     if player_choice == "Paper" and pc_choice == "Rock":
          print("You win!", player_choice, "covers", pc_choice)
     elif player_choice == "Paper" and pc_choice == "Scissors":
          print("You lose!", pc_choice, "cuts", player_choice)
     elif player_choice == "Paper" and pc_choice == "Paper":
          print("It's a draw!")

     """Scissors"""
     if player_choice == "Scissors" and pc_choice == "Rock":
          print("You lose!", pc_choice, "smashes", player_choice)
     elif player_choice == "Scissors" and pc_choice == "Scissors":
          print("It's a draw!")
     elif player_choice == "Scissors" and pc_choice == "Paper":
          print("You win!", player_choice, "cuts", pc_choice)

while 
Choice(player_choice)
print("Player has chosen: ", player_choice)
print("PC has chosen: ", pc_choice)
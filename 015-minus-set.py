# "Substract" one set from another

set1 = {'A', 'O', 'E', 'I', 'U'}
set2 = {'K', 'L', 'M', 'N', 'O'}

set3 = set1 - set2 
print(set3)  # prints set1 without O

set4 = set2 - set1
print(set4)  # prints set2 without O

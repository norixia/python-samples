# "+" is an overloaded (redefined) operator
# It behaves differently with strings and numbers

a, b = 5, 6
str1, str2 = "5", "6"

print(a + b)         # (int) 5+6=11
print(str1 + str2)   # (str) 56

# Rock-Paper-Scissors game 
# src: https://thehelloworldprogram.com/python/python-game-rock-paper-scissors/
"""Rules:
     - Scissors beats Paper
     - Rock beats Scissors
     - Paper beats Rock
     - both players choose the same -> draw

    Two players:
     - Player A (player) must enter an input
     - Player B (pc) chooses randomly
"""
     
from random import randint

# create a list of play options 
choice = ["Rock", "Paper", "Scissors"]

# assign a random play to the computer
pc = choice[randint(0,2)]

# set player to False
player = False

while player == False:
    # set player to True
    player = input("Rock, Paper, Scissors?")
    if player == pc:
        print("draw!")

    elif player == "Rock":
        if pc == "Paper":
            print("You lose!", pc, "covers", player)
        else:
            print("You win!", player, "smashes", pc)

    elif player == "Paper":
        if pc == "Scissors":
            print("You lose!", pc, "cuts", player)
        else:
            print("You win!", player, "covers", pc)

    elif player == "Scissors":
        if pc == "Rock":
            print("You lose!", pc, "smashes", player)
        else:
            print("You win!", player, "cuts", pc)
    
    else:
        print("That's not a valid play. Please check your spelling!")

    #player was set to True, but we want it to be False so the loop continues
    player = False
    pc = choice[randint(0,2)]
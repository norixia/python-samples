string = '''This is a multi-line
string. You can continue it on
another line by pressing the 
Enter key'''
print(string)

string2 = '''Notice the spaces
          on the left. Are they
          gonna appear in the output? '''
print(string2)
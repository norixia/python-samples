# Reading command-line arguments
# run it with python3 019-stdin.py John

import sys

name = sys.argv[1]
print("Hello, " + name + "!")
# Testing "empty" values in boolean context
# When it comes to boolean context, (Zero number / Empty string) is treated as False
# However, a string that contains zero is considered to be True in boolean context

# Zero number
x = 0
if x:
    print('True')
else:
    print('False')

# Empty string
y = ''
if y:
    print('True')
else:
    print('False')

# String containing 0
s = '0'
if s:
    print('True')
else:
    print('False')
# Using if, elif (else if) and else

x = int(input('Enter a value for x: '))

if x < 5:
    print(f'{x} is less than 5')
elif x < 10:
    print(f'{x} is less than 10')    
else:
    print(f'{x} is 10 or above')
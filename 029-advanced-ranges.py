# Using the third parameter in a range
#         01234567890123
string = "abcdefghijklmn"

# Print every second character
print(string[::2])

# Print every third character
print(string[::3])
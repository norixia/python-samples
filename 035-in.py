# Using "in"
# It can check if the character is in a string;
# It can check if it is an element of a list or a set.
# You can try with dictionaries.

# with strings
print('s' in 'Water')     # False
print('s' in 'Wasser')    # True

# with lists
my_list = [1, 2, 3, 4, 5]
print(4 in my_list)       # True

# with sets
my_set = {1, 2, 3, 4, 5}
print(4 in my_set)        # True
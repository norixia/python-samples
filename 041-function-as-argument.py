# function used as an argument of another function

# add two numbers
def add(x,y):
    return x + y

# multiply two numbers
def multiply(x,y):
    return x * y

# execute a function twice
def do_twice(func, x, y):
    return func(func(x,y), func(x,y)) # add (add (5, 10), add (5, 10))

a = 5
b = 10

print(do_twice(add, a, b))
# Deleting a variable

x = 56
print(x)

# Now delete it
del(x)
print(x)  # won't be able to print
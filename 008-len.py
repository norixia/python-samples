# The built-in "len" function
# it returns the number of items

list_var = [1, 2, 3, 4, 5]
str_var = "Hello, World"
dict_var = {"a" : 100, "b" : 200}

print(len(list_var))  # 5
print(len(str_var))   # 12
print(len(dict_var))  # 2
# Get the beginning or the end of a string using ranges
#         01234567890123
string = "abcdefghijklmn"

# beginning
print(string[:5])

# end
print(string[10:])
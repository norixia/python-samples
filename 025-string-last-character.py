# Getting the last character of a string

string = "Hello, World!"
# -->                 ^

print(string[-1])
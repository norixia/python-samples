# Task: Create a game of Rock-Paper-Scissors
""" Rules:
     - Scissors beats Paper
     - Rock beats Scissors
     - Paper beats Rock
     - both players choose the same -> draw

    The game asks if the user wants to play or not -> 2 Modes

    Player_vs_PC Mode - Two players:
     - Player A (user) gives input for his choice
     - Player B (pc) chooses randomly

    PC_vs_PC Mode - Two players:
     - Player A (pc) always chooses Paper
     - Player B (pc) chooses randomly

    The game can have as many rounds b/n the two players as the user wants.
    The output/result of the program should be similar to the following:
     - "Player A wins 31 out 100 games"
     - "Player B wins 37 out 100 games"
     - "draw: 32 out 100 games"
"""

import random
import os

# method to clear the screen
clear = lambda: os.system('cls')

# a list of play options 
OPTIONS = ["Rock", "Paper", "Scissors"]

# results
PLAYER_A_WINS = 0
PLAYER_B_WINS = 0
DRAWS = 0
TOTAL_MATCHES = 0


def Player_Option():
    """Receives the imput from the user and checks if it is valid."""
    while True:
        player_choice = input("Choose from 'Rock', 'Paper' and 'Scissors': ")
        if player_choice in ["Rock", "rock", "r", "R"]:
            player_choice = "Rock"
            break
        elif player_choice in ["Paper", "paper", "p", "P"]:
            player_choice = "Paper"
            break
        elif player_choice in ["Scissors", "scissors", "s", "S"]:
            player_choice = "Scissors"
            break
        else:
            print("That's not a valid input for the game. Please try again!")
    return player_choice


def Player_vs_Pc():
    """Enables the Player_vs_Pc Mode:
       - Player_a is played by the user
       - Uses Player_Option() to receive and validatethe user input
       - Player_b is played by the PC and always picks at random
       - finally the game begins
    """
    clear()
    print("You now play vs the computer!")
    player_a = Player_Option()
    player_b = random.choice(OPTIONS)
    print("")
    print("You chose: ", player_a)
    print("PC chose: ", player_b)
    Game(player_a, player_b)
    

def Pc_vs_PC():
    """Enables the Pc_vs_Pc Mode:
        - Player_a and Player_b are played by the PC;
        - Player_a chooses always 'Paper'
        - Player_b always picks at random
        - finally the game begins
    """
    clear()
    print("The computer now plays with itself!")
    # the following executes 100 games every time the player refuses to play
    # you can remove numberOfGames, currentGame and the while loop to play 1 game before asking again
    currentGame = 0 
    numberOfGames = int(input("How many games do you want for the Pc to play? "))
    # numberOfGames = 100
    while currentGame < numberOfGames:
        player_a = OPTIONS[1]
        player_b = random.choice(OPTIONS)
        print("Pc_vs_Pc: CPU-a chose: ", player_a)
        print("Pc_vs_Pc: CPU-b chose: ", player_b)
        Game(player_a, player_b)
        clear() # remove if you want to stop deleting console
        currentGame += 1 


def Game(player_a, player_b):
    """The game itself and its rules:
        - Scissors beats Paper
        - Rock beats Scissors
        - Paper beats Rock
        - both players choose the same -> draw
    """
    print("")
    print("The game has begun!")
    # print(player_a)
    # print(player_b)

    # results
    global PLAYER_A_WINS
    global PLAYER_B_WINS
    global DRAWS
    global TOTAL_MATCHES
    TOTAL_MATCHES += 1
    
    # Both players picked the same option
    if player_a == player_b:
        print("It's a draw!")
        DRAWS += 1

    # Player_a chose "Rock"
    elif player_a == "Rock":
        if player_b == "Paper":
            print("Player_A loses from Player_B!", player_b, "beats", player_a)
            PLAYER_B_WINS += 1
        else:
            print("Player_A wins over Player_B!", player_a, "beats", player_b)
            PLAYER_A_WINS += 1

    # Player_a chose "Paper"
    elif player_a == "Paper":
        if player_b == "Scissors":
            print("Player_A loses from Player_B!", player_b, "beats", player_a)
            PLAYER_B_WINS += 1
        else:
            print("Player_A wins over Player_B!", player_a, "beats", player_b)
            PLAYER_A_WINS += 1
    
    # Player_a chose "Scissors"
    elif player_a == "Scissors":
        if player_b == "Rock":
            print("Player_A loses from Player_B!", player_b, "beats", player_a)
            PLAYER_B_WINS += 1
        else:
            print("Player_A wins over Player_B!", player_a, "beats", player_b)
            PLAYER_A_WINS += 1



def main():
    print("+--------------------------------------------+")
    print("| Welcome to the game of Rock-Paper-Scissors |")
    print("+--------------------------------------------+\n")
    
    while True:
        print("")
        player_input = input("Do you want to play or exit and check the results? (yes/no/result): ")
        if player_input in ["Y", "y", "Yes", "yes"]:
            Player_vs_Pc()
        elif player_input in ["Results","results","Result","result","exit", "Exit", "stop", "Stop"]:
            print("")
            print("The results of the game are:")
            print(" - Player_A has won {} of {} games.".format(PLAYER_A_WINS, TOTAL_MATCHES))
            print(" - Player_B has won {} out of {} games.".format(PLAYER_B_WINS, TOTAL_MATCHES))
            print(" - Draws is {} out of {} games.".format(DRAWS, TOTAL_MATCHES))
            print("")
            break
        elif player_input in ["N", "n", "No", "no"]:
            Pc_vs_PC()
        else:
            print("You have not entered a valid command!")


if __name__ == '__main__':
    main()

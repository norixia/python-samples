#! /usr/bin/env python3
# this is my first game in python
# you can run this directly from cmd, write "python FirstGame.py"
# Alternative: 
# you can run this from cmd or power shell and write "python" then "exec(open("./FirstGame.py").read())"

import pygame

# initialize the pygame
pygame.init()

# create a window to draw on
screenWidth = 500
screenHeight = 500
win = pygame.display.set_mode((screenWidth, screenHeight))

# title of the game window
pygame.display.set_caption("UwU")

# initial positions for x and y
x = 300
y = 300

charWidth = 40   # for rect
charHeight = 60  # for rect
charRadius = 10  # for circle
charVelocity = 5 # for any shape

run = True
while run:
    pygame.time.delay(100)  # delay between iterations needed

    # check for the events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:  # if exit button is clicked
            run = False                # break the loop and exit the game

    keys = pygame.key.get_pressed()

    # move with left keys + border on the left side of the screen 
    if (keys[pygame.K_LEFT] or keys[pygame.K_a]) and x > charVelocity: 
        x -= charVelocity

    # move with right keys + border on the right side of the screen
    if (keys[pygame.K_RIGHT] or keys[pygame.K_d]) and x < screenWidth - charWidth - charVelocity:
        x += charVelocity

    # move with up keys + border on the upper side of the screen
    if (keys[pygame.K_UP] or keys[pygame.K_w]):
        y -= charVelocity

    # move with down keys + border on the down side of the screen
    if (keys[pygame.K_DOWN] or keys[pygame.K_s]):
        y += charVelocity

    win.fill((0,0,0))
    
    # draw a rectangle, the coordinate is up-left corner of it
    pygame.draw.rect(win, (0,120,0), (x,y,charWidth,charHeight)) 

    # draw a circle, the coordinate is the center of the circle
    # pygame.draw.circle(win, (0,120,0), (x,y), radius, radius)   
    
    pygame.display.update()

# End the game and close the window
pygame.quit()  

# The "*" operator is also overloaded (redefined) for strings

string = "A"
long_string = string * 15
print(long_string)
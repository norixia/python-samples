# different data types in the same variable
x = 56
print(56)
print(type(x))

x = "string"
print(x)
print(type(x))
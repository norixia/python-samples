# Chained condition checks

x = 10
y = 20
z = 30

if x < y < z:       # instead of x < y and y < z
    print('OK')
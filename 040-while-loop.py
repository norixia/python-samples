# Using while-loop

# Simple form of while-loop
x = 0
while x < 4:
    print(x)
    x += 1

print() # new line

# The "else" clause can be used with "while"
y = 0
while y < 5:
    print(y)
    y += 1
else:
    print('Done')
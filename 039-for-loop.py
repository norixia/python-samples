# Using for-loop

# Loop over a list 
my_list = [3, 5, 7, 9]
print('my_list consists of the following: ')
for x in my_list:
    print(x)

print() # print a new line

# Loop over a string 
my_string = "Hello, World!"
print('my_string consists of the following: ')
for ch in my_string:
    print(ch)

print() # print a new line

# Loop over a dictionary 
# When iterating, you get the keys of the dictionary
data = {
    'England': 'London',
    'France': 'Paris',
    'Germany': 'Berlin',
    'Bulgaria': 'Sofia',
    'Italy': 'Rome',
    'Switzerland': 'Bern'
}
for country in data:
    print('The capital of ' + country + 
          ' is ' + data[country])


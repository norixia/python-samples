# Multi-dimentional lists

my_list = [
    [1, 3, 5],
    [7, 9, 11],
    [13, 15, 17]
]

print(my_list)
print(my_list[1])     # [7, 9, 11]
print(my_list[1][1])  # 9
print(my_list[2][2])  # 17
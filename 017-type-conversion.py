# Converting types of variables 
# or type casting (C++ terminology)

a, b = 5, 6
c, d = "5", "6"

print(str(a) + str(b))   # converts to str --> 56
print(int(c) + int(d))   # converts to int --> 5+6 = 11
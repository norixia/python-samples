# What is there in the 0th command-line argument?
# run it with python3 020-argv0.py a b c

import sys
whatisthere = sys.argv[0]
print(whatisthere)   # it gives you the name of the program
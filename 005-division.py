# integer vs floating point division

print(10 / 3)    # (float) 3.3333333333333335 
print(10 // 3)   # (int)   3
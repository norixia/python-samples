# Be careful with indentation
# In python the white spaces can make a difference
# For example, you can end the "if" block and can return to the outer scope as demonstrated

if 100 > 1000:
    print('100 > 1000??')
print('100 > 1000!')
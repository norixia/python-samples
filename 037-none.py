# Be carefule when you use "None" in boolean operations

a = True and None
print(a)    # None

b = True or None
print(b)    # True

c = False and None
print(c)    # False

d = False or None
print(d)    # None

print()
# Also, when using "None", the order is important
print(False and None)
print(None and False)
# simple use of a function

# Alternative 1: define function f()
def f(x):
    print(x**2)  # x (to the power of) 2

# Alternative 2: define function square()
def square(x):
    y = x ** 2   # x (to the power of) 2
    return y


# call function f()
f(3)

# call function square()
result = square(3)
print(result)
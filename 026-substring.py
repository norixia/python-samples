# Getting a substring

#         0123456789012
string = "Hello, World!"
# -->            ^^^^^

# We want to print only "World"
str_world = string[7:12]
print(str_world)
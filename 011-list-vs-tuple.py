# Tuples vs lists
# Similar, yet different https://www.afternerd.com/blog/difference-between-list-tuple/

list_var = [2, 4, 6, 8]     # list can be modified, after its creation
tuple_var = (2, 4, 6, 8)    # tuple cannot be modified, after its creation 

print(type(list_var))
print(type(tuple_var))

# list modification is possile
print(id(list_var))
list_var[0] = 1
print(list_var)
print(id(list_var))

# tuple modification is not possible
# it creates another object with different memory address
print(id(tuple_var))
tuple_var = (1, 4, 6, 8)
print(tuple_var)
print(id(tuple_var))
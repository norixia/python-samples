# non-trivial assignments

x = y = 100
print (x)  # x = 100
print (y)  # y = 100

a, b = 3, 4
print(a)   # a = 3
print(b)   # b = 4
# How does the "+" operator work with lists and tuples?

a = (1, 3, 5)
b = (7, 9, 11)
c = a + b       # concatenate lists
print(c)

d = [2, 4, 6]
e = [8, 10, 12]
f = d + e       # concatenate tuples

print(f)
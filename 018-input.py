# Asking the user to enter data

name = input("What's your name? ")
print("Hello, " + name + "!")
print(f"How are you, {name}?")
print("{}, are you free on Friday?" .format(name))
print("%s, do you want to watch a movie?" % (name))
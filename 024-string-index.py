# Indexing characters in a string

#         0123456789...
string = "Hello, World!"

print(string[0])  # H
print(string[1])  # e
print(string[5])  # ,
print(string[8])  # o

print("--------")
for i in string:  
    print(i)      # prints the whole string char by char
print("--------")
# How to split a string to characters

string = "Hello, World!"

# First, print a character
print(type(string))
print(string[3])

# Now, make a list out of a string
# Just convert a type:
chars = list(string)
print(type(chars))
print(chars[3])
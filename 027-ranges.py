# Using ranges with strings
#         01234567890123
string = "abcdefghijklmn"

# Characters from 3rd to 5th
print(string[3:6])   # 6th is not included
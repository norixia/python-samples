# Task: Create a game of Rock-Paper-Scissors
# src: https://www.youtube.com/watch?v=-JACmC8kabo
""" Rules:
     - Scissors beats Paper
     - Rock beats Scissors
     - Paper beats Rock
     - both players choose the same -> draw

    Two players:
     - Player A (player) must enter an input
     - Player B (pc) chooses randomly
"""
from random import randint

pc_wins = 0
player_wins = 0

def Player_Option():
    player_choice = input("Choose from Rock, Paper or Scissors: ")
    if player_choice in ["Rock", "rock", "r", "R"]:
        player_choice = "r"
    elif player_choice in ["Paper", "paper", "p", "P"]:
        player_choice = "p"
    elif player_choice in ["Scissors", "scissors", "s", "S"]:
        player_choice = "s"
    else:
        print("That's not a valid input for the game. Please try again!")
        Player_Option()
    return player_choice

def PC_Option():
    pc_choice = randint(1,3)
    if pc_choice == 1:
        pc_choice = "r"
    elif pc_choice == 2:
        pc_choice = "p"
    elif pc_choice == 3:
        pc_choice = "s"
    return pc_choice


while True:
    print ("")
    player_choice = Player_Option()
    pc_choice = PC_Option()
    print("")

    if player_choice == "r":
        if pc_choice == "r":
            print("Player chose 'rock'. The pc chose 'rock'. It is a draw!")
        elif pc_choice == "p":
            print("Player chose 'rock'. The pc chose 'paper'. You lose!")
            pc_wins += 1
        elif pc_choice == "s":
            print("Player chose 'rock'. The pc chose 'scissors'. You win!")
            player_wins += 1

    elif player_choice == "p":
        if pc_choice == "p":
            print("Player chose 'paper'. The pc chose 'paper'. It is a draw!")
        elif pc_choice == "s":
            print("Player chose 'paper'. The pc chose 'scissors'. You lose!")
            pc_wins += 1
        elif pc_choice == "r":
            print("Player chose 'paper'. The pc chose 'rock'. You win!")
            player_wins += 1

    elif player_choice == "s":
        if pc_choice == "s":
            print("Player chose 'scissors'. The pc chose 'scissors'. It is a draw!")
        elif pc_choice == "r":
            print("Player chose 'scissors'. The pc chose 'rock'. You lose!")
            pc_wins += 1
        elif pc_choice == "p":
            print("Player chose 'scissors'. The pc chose 'paper'. You win!")
            player_wins += 1


    print("")
    print("Player wins: " + str(player_wins))
    print("Pc wins: " + str(pc_wins))
    print("")

    player_choice = input("Do you want to continue playing? (y/n) ")
    if player_choice in ["Y", "y", "Yes", "yes"]:
        pass
    elif player_choice in ["N", "n", "No", "no"]:
        break
    else: 
        break